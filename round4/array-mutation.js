let input = [1, 2, 3, 4];
//const indices = [3, 2, 1, 0];
let indices = [1, 3, 0, 2]; //[3,1,4,2]

function mutation(input, indices) {
  let tmp = input.slice();

  indices.forEach((newIndex, oldIndex) => input[newIndex] = tmp[oldIndex]);

  return input;
}

console.log(mutation(input, indices));

function mutationInPlace(input, indices) {
  function swap(a, i, j) {
    let tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }

  let swapped = true;

  while(swapped) {
    swapped = false;
    for(let i = 0;i < indices[i];i++) {
      swap(input, i, indices[i]);
      swap(indices, i, indices[i]);
      swapped = true;
    }
  }

  return input;
}

input = [1, 2, 3, 4];
indices = [1, 3, 0, 2]; //[3,1,4,2]
console.log(mutationInPlace(input, indices));
