const Emitter = function() {
  this.events = {};
};

Emitter.prototype.subscribe = function(eName, cb) {
  this.events[eName] = this.events[eName] || [];
  this.events[eName].push(cb);

  return {
    release: () => {
      let index = this.events[eName].indexOf(cb);
      this.events[eName].splice(index, 1);
      if (!this.events[eName].length) delete this.events[eName];
    }
  }
};

Emitter.prototype.emit = function(eName, ...args) {
  if (!this.events[eName]) throw new Error(`Event ${eName} not found`);

  this.events[eName].forEach(cb => cb.apply(this, args));
};

let emitter = new Emitter();
let sub1 = emitter.subscribe('test', (a, b) => console.log('cb1', a, b));
let sub2 = emitter.subscribe('test', (a, b) => console.log('cb2', a, b));
emitter.emit('test', 'hello', 'world');
console.log(emitter);
sub1.release();
sub2.release();
console.log(emitter);
