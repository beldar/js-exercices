function getPath(root, node) {
  let path = [], parent, index;

  while (node !== root) {
    parent = node.parentNode;
    index = Array.from(parent.childNodes).indexOf(node);
    path.push(index);
    node = parent;
  }

  return path;
}

function followPath(root, path) {
  let index, node = root;

  while (path.length) {
    index = path.pop();
    node = node.childNodes[index];
  }

  return node;
}

// Recursive

function getPath(root, node, path) {
  path = path || [];

  if (node !== root) {
    parent = node.parentNode;
    index = Array.from(parent.childNodes).indexOf(node);
    path.push(index);
    return getPath(root, parent, path);
  } else {
    return path;
  }
}

function followPath(node, path) {
  if (path.length) {
    let index = path.pop();
    return followPath(node.childNodes[index], path)
  } else {
    return node;
  }
}
