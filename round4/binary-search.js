const input = [1, 2, 3, 4];
const target = 5;

function bsearch(arr, target) {
  let min = 0,
      max = arr.length - 1,
      mid;

  while(max >= min) {
    mid = Math.floor((max+min)/2);
    if (arr[mid] > target) {
      max = mid - 1;
    }
    else if(arr[mid] < target) {
      min = mid + 1;
    } else return mid
  }

  return -1;
}

console.log(bsearch(input, target));
