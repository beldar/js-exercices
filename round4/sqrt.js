
function sqrtIt(n) {
  let guess = n/2.0;
  let newGuess = ((n/guess)+guess)/2.0;

  while (newGuess !== guess) {
    guess = newGuess;
    newGuess = ((n/guess)+guess)/2.0;
  }

  return guess;
}

function sqrtRec(n, guess) {
  guess = guess || n/2.0;
  let newGuess = ((n/guess)+guess)/2.0;

  if (guess !== newGuess) {
    return sqrtRec(n, newGuess)
  }

  return newGuess;
}

console.log(sqrtIt(50));

console.log(sqrtRec(50));
