function debounce(fn, wait) {
  let timeout = null;

  return function() {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn.apply(this, arguments), wait);
  };
};

var dbounced = debounce((t) => console.log(t), 200);

dbounced(1);
dbounced(2);
dbounced(3);
