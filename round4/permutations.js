function permutations(a) {
  let ret = [];

  if (a.length === 1) {
    ret.push(a);
    return ret;
  }

  a.forEach((el, i) => {
    let rest = a.slice(0, i).concat(a.slice(i+1));
    let others = permutations(rest);
    others.forEach(per => ret.push([el].concat(per)));
  });

  return ret;
}

console.log(permutations([1, 2, 3]));
