// Write a function that finds all possible permutations
// of the values in an array

// https://en.wikipedia.org/wiki/Heap%27s_algorithm

const swap = ( arr, i, j ) => {
  let m = arr[i];
  arr[i] = arr[j];
  arr[j] = m;
};


function heapsRecursive(arr) {
  const results = [];

  const generate = ( n, arr ) => {
    if ( n == 1 ) {
      results.push( arr.slice() );
    } else {
      for ( let i = 0; i < n - 1; i++) {
        generate( n - 1, arr );
        swap( arr, n%2 ? 0 : i, n - 1 );
      }
      generate( n - 1, arr )
    }
  }

  generate( arr.length, arr );
  return results;
}

console.time('recursive')
console.log(heapsRecursive([1,2,3]));
console.timeEnd('recursive');

/*
procedure generate(n : integer, A : array of any):
    if n = 1 then
          output(A)
    else
        for i := 0; i < n - 1; i += 1 do
            generate(n - 1, A)
            if n is even then
                swap(A[i], A[n-1])
            else
                swap(A[0], A[n-1])
            end if
        end for
        generate(n - 1, A)
    end if
 */

function heapsNonRecursive( arr ) {
  const n = arr.length;
  const results = [];
  let c = Array( n ).fill( 0 );

  results.push( arr.slice() );

  let i = 0;

  while( i < n ) {
    if ( c[ i ] < i ) {
      swap( arr, i % 2 ? c[ i ] : 0, i );
      results.push( arr.slice() );
      ++c[ i ];
      i = 0;
    } else {
      c[ i ] = 0;
      i++;
    }
  }

  return results;
}

console.time('nonrecursive');
console.log(heapsNonRecursive([1,2,3]));
console.timeEnd('nonrecursive');

function* heapsGenerator( arr ) {
  var length = arr.length,
      c = Array(length).fill(0),
      i = 1;

  yield arr.slice();
  while (i < length) {
    if (c[i] < i) {
      swap( arr, i % 2 ? c[ i ] : 0, i );
      ++c[i];
      i = 1;
      yield arr.slice();
    } else {
      c[i] = 0;
      ++i;
    }
  }
}

console.time('generator');
console.log([...heapsGenerator([1,2,3])]);
console.timeEnd('generator');

/*
procedure generate(n : integer, A : array of any):
    c : array of int

    for i := 0; i < n; i += 1 do
        c[i] := 0
    end for

    output(A)

    i := 0;
    while i < n do
        if  c[i] < i then
            if i is even then
                swap(A[0], A[i])
            else
                swap(A[c[i]], A[i])
            end if
            output(A)
            c[i] += 1
            i := 0
        else
            c[i] := 0
            i += 1
        end if
    end while

 */
