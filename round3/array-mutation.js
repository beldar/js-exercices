// Given an input array and another array that describes a new
// index for each element, mutate the input array so that each element ends up in their new index.
// Discuss the runtime of the algorithm and how you can be sure there won't be any infinite loops.

const input = [1, 2, 3, 4];
//const indices = [3, 2, 1, 0];
const indices = [1, 3, 0, 2]; //[3,1,4,2]

function mutationWithTmp(input, indices) {
  let tmp = input.slice();

  indices.forEach((newIndex, i) => {
    input[newIndex] = tmp[i]
  });

  return input;
}

function mutation(input, indices) {
  const swap = function(arr, i, j) {
    let tmp = arr[j];
    arr[j] = arr[i];
    arr[i] = tmp;
  };

  let swapped = true;
  while ( swapped ) {
    swapped = false;
    for (let i = 0;i < indices[i];i++) {
      swap(input, i, indices[i]);
      swap(indices, i, indices[i]);
      swapped = true;
    }
  };
}

//console.log(mutationWithTmp(input, indices));
console.time('bubbleSort');
console.log('input', input);
console.log('indices', indices);
console.log(mutation(input, indices));
console.log(input);
console.timeEnd('bubbleSort');
