const a = [1, [2], [3, [4, [5]]], 6, [7, 8]];

function flattenRecursive(a, ret) {
  return a.reduce((ret, el) => {
    if (!Array.isArray(el)) {
      ret.push(el);
    } else {
      flattenRecursive(el, ret);
    }
    return ret;
  }, ret || []);
};

console.log(flattenRecursive(a));

function flattenIterative(a) {
  let ret = [];
  while(a.length) {
    let cur = a.pop();
    if (!Array.isArray(cur)) {
      ret.push(cur);
    } else {
      a = [...a, ...cur];
    }
  }

  return ret.reverse();
}

console.log(flattenIterative(a));
