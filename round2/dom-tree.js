let rootA = document.getElementById('a'),
	  rootB = document.getElementById('b'),
	  nodeA = document.querySelector('#a li:last-of-type');

console.log(nodeA);

function getPath(root, node) {
	let path = [];
	while (root !== node) {
		let parent = node.parentNode;
		path.push(Array.from(parent.childNodes).indexOf(node));
		node = parent;
	}

	return path;
}

function followPath(root, path) {
	let node = root;
	while (path.length) {
		node = node.childNodes[path.pop()];
	}

	return node;
}

let nodeB = followPath(rootB, getPath(rootA, nodeA));

console.log(nodeB);
