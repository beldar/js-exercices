//get all permutations of an array

function permutations(a) {
  let res = [];

  if (a.length === 1) {
    res.push(a);
    return res;
  }

  a.forEach((el, i) => {
    let rest = a.slice(0, i).concat(a.slice(i+1, a.length));
    let others = permutations(rest);
    others.forEach(per => res.push([el].concat(per).join('')));
  });

  return res;
}

console.log(permutations(['a', 'b', 'c']));
