// Given an input array and another array that describes a new
// index for each element, mutate the input array so that each element ends up in their new index.
// Discuss the runtime of the algorithm and how you can be sure there won't be any infinite loops.

let input = [1, 2, 3, 4];
//const indices = [3, 2, 1, 0];
let indices = [1, 3, 0, 2]; //[3,1,4,2]

// With extra space
// function mutate(input, indices){
//   var temp = input.slice();
//   indices.forEach(function(newIndex, oldIndex){
//       input[newIndex] = temp[oldIndex];
//   });
// }


// Without extra space
//  It works by sorting the index array
//  and every time something in the index array is moved,
//  it moves the same elements on the original array
function mutate(arr,indexes) {
  const swap = function(a, i, j) {
    var temp = a[j];
    a[j] = a[i];
    a[i] = temp;
  };

  let swapped = true;
  while (swapped) {
    swapped = false;
    for (var i = 0; i < indexes[i];i++) {
      swap(arr,i,indexes[i]);
      swap(indexes, i, indexes[i]);
      swapped = true;
    }
  }
};

console.time('bubbleSort');
console.log('input', input);
console.log('indices', indices);
console.log(mutate(input, indices));
console.log(input);
console.timeEnd('bubbleSort');
