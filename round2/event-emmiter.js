const Emitter = function() {
  this.events = {}
};

Emitter.prototype.subscribe = function( eName, cb ) {
  this.events[eName] = this.events[eName] || [];
  this.events[eName].push(cb);

  return {
    unsubscribe: () => {
      let index = this.events[eName].indexOf(cb);
      this.events[eName].splice(index, 1);
    }
  }
}

Emitter.prototype.emmit = function( eName, ...args) {
  if (!this.events[eName]) return;

  this.events[eName].forEach(cb => cb.apply(this, args));
};

var emitter = new Emitter();

var s1 = emitter.subscribe('test', (a, b) => console.log('cb1', a, b) );
var s2 = emitter.subscribe('test', (a, b) => console.log('cb2', a, b) );

emitter.emmit('test', 'hello', 'world');

console.log(emitter);
s1.unsubscribe();
s2.unsubscribe();
console.log(emitter);
