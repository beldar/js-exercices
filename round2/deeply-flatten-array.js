function flattenRecursive( a ) {
  let result = [];

  function flatten(a, result) {
    if (!Array.isArray(a)) return result.push(a);
    a.forEach(el => flatten(el, result))
  }

  flatten(a, result);

  return result;
}

function flattenIterative( a ) {
  let res = [];

  while( a.length ) {
    let cur = a.pop();
    if (!Array.isArray(cur)) {
      res.push(cur)
    } else {
      a = [...a, ...cur];
    }
  }

  return res.reverse();
}

// If array can only contain numeric values
function flattenNumeric( a ) {
  return a.toString().split(",").map(Number)
}

let a = [1, [2, 3, [4]], [5], [6], [[7], [8, [9]]]];
console.time('flattenIterative');
console.log(flattenIterative(a));
console.timeEnd('flattenIterative');

a = [1, [2, 3, [4]], [5], [6], [[7], [8, [9]]]];
console.time('flattenRecursive');
console.log(flattenRecursive(a));
console.timeEnd('flattenRecursive');

a = [1, [2, 3, [4]], [5], [6], [[7], [8, [9]]]];
console.time('flattenNumeric');
console.log(flattenNumeric(a));
console.timeEnd('flattenNumeric');
