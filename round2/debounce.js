function debounce(fn, wait) {
  let timeout = null;

  return function() {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn.apply(this, arguments), wait);
  }
};

var debounced = debounce((t) => console.log(t), 200);

debounced('hello world 1');
debounced('hello world 2');
debounced('hello world 3');
