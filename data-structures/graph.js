const Graph = () => Object.create({
  nodes: {},

  addEdge(e) {
    this.nodes[e] = [];
    return this;
  },

  addVertex(e, v) {
    this.nodes[e] = this.nodes[e] || [];
    this.nodes[e].push(v);
    return this;
  },

  DFS(source) {
    let visited = {};

    const _dfs = (node) => {
      if (!node || !this.nodes[node]) return;

      visited[node] = true;
      console.log(node);

      this.nodes[node].forEach(n => !visited[n] ? _dfs(n) : null );
    };

    source = source || Object.keys(this.nodes)[0];

    return _dfs(source);
  },

  BFS(source) {
    source = source || Object.keys(this.nodes)[0];
    let q = [];
    let visited = {};
    visited[source] = true;
    q.push(source);

    while(q.length) {
      let cur = q.shift();
      console.log(cur);

      (this.nodes[cur] || []).forEach(v => {
        if (!visited[v]) {
          visited[v] = true;
          q.push(v);
        }
      });
    }
  }
});

module.exports = Graph;

// const g = Graph();
//
// g.addEdge(0)
// .addEdge(1)
// .addEdge(2)
// .addEdge(3)
// .addEdge(4)
// .addEdge(5);
//
// g.addVertex(0,1)
// .addVertex(0,4)
// .addVertex(0,5)
// .addVertex(1,3)
// .addVertex(1,4)
// .addVertex(2,1)
// .addVertex(3,2)
// .addVertex(3,4);
//
// console.log(g.nodes);
//
// console.log('---- DFS ----');
// g.DFS();
//
// console.log('---- BFS ----');
// g.BFS();
