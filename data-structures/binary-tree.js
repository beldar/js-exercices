const Node = function(key) {
	this.left = null;
	this.right = null;
	this.parent = null;
	this.key = key || null;
};

const BinaryTree = () => Object.create({
	root: null,
	ntotal: 0,

	height(node = this.root) {
		if(!node) return -1;
		else return Math.max(this.height(node.left), this.height(node.right))+1
	},

	insert(z) {
		let y = null,
				x = this.root;

		while(x) {
			y = x;
			if (z.key < x.key) x = x.left;
			else x = x.right;
		}

		z.parent = y;
		if (!y) this.root = z;
		else if(z.key < y.key) y.left = z;
		else y.right = z;

		this.ntotal++;
	},

	transplant(u, v) {
		if(!u.parent) this.root = v;
		else if(u == u.parent.left) u.parent.left = v;
		else u.parent.right = v;

		if(v) v.parent = u.parent;
	},

	delete(z) {
		if(!z.left) this.transplant(z, z.right);
		else if(!z.right) this.transplant(z, z.left);
		else {
			let y = this.minimum(z.right);
			if(y.parent != z) {
				this.transplant(y, y.right);
				y.right = z.right;
				y.right.parent = y;
			}
			this.transplant(z, y);
			y.left = z.left;
			y.left.parent = y;
		}
	},

	search(target, node = this.root) {
		if (!node || node.key === target) return node;
		if (target < node.key) return this.search(target, node.left);
		return this.search(target, node.right);
	},

	minimum(node = this.root) {
		while(node && node.left) node = node.left;
		return node;
	},

	maximum(node = this.root) {
		while(node && node.right) node = node.right;
		return node;
	},

	inorder(node = this.root, indent = 1) {
		if(node) {
			this.inorder(node.left, indent+1);
			console.log(Array(indent).join('--') + '| ' + node.key);
			this.inorder(node.right, indent+1);
		}
	},

	print() {
		let h = 0,
				n = Object.assign(this.root, {lvl: h}),
				q = [], lvls = [], separator = '  ';

		console.log('-----------');
		while(n) {
			lvls[n.lvl] = lvls[n.lvl] || [];
			lvls[n.lvl].push(n.key);
			h++;
			if(n.left) q.push(Object.assign(n.left, {lvl: h, isLeft:true}));
			if(n.right) q.push(Object.assign(n.right, {lvl: h, isRight:true}));
			n = q.shift();
		}

		lvls = lvls.filter(x => x.length);
		let c = lvls.length;
		let extra;
		lvls.forEach((lvl, i) => {
			if (!lvl.length) return;
			console.log(separator.repeat(c-i) + lvl.join(separator.repeat(c-i)));
		});
		console.log('-----------');
	}
});

module.exports = BinaryTree;

// const t = BinaryTree();
// t.insert(new Node(2));
// t.insert(new Node(4));
// t.insert(new Node(3));
// t.insert(new Node(1));
// t.insert(new Node(5));
// console.log('-----------');
// t.inorder();
// console.log('-----------');
// console.log('Min', t.minimum().key);
// console.log('Max', t.maximum().key);
// let four = t.search(4);
// console.log('Search', four.key);
// t.delete(four);
// console.log('Delete 4');
// console.log('-----------');
// t.inorder();
// console.log('-----------');
// console.log('Insert 6');
// t.insert(new Node(6));
// console.log('-----------');
// t.inorder();
// console.log('-----------');
// console.log('Insert 5');
// t.insert(new Node(5));
// console.log('-----------');
// t.inorder();
// console.log('-----------');
