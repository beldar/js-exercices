// Given an max number N, count the total of prime numbers between 0 and N

var countPrimes = function(n) {
    function isPrime(n) {
     if (isNaN(n) || !isFinite(n) || n%1 || n<2) return false;
     var m=Math.sqrt(n);
     for (var i=2;i<=m;i++) if (n%i==0) return false;
     return true;
    }

    var count = 0;

    while(n > 0) {
        if (isPrime(n)){
            count++;
            console.log('Prime',n);
        }
        n--;
    }

    return count;
};

console.log(countPrimes(2));
