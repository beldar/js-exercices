// Implement a function to shuffle an array

function shuffle(a) {
  let res = [];
  let mid = Math.floor(a.length/2);

  let firstHalf = a.slice(0, mid);
  let secondHalf = a.slice(mid, a.length);

  while(firstHalf.length && secondHalf.length) {
    res.push(firstHalf.shift());
    res.push(secondHalf.shift());
  }

  if (firstHalf.length) res = res.concat(firstHalf);
  if (secondHalf.length) res = res.concat(secondHalf);

  return res;
}

const a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

console.log(shuffle(a));
