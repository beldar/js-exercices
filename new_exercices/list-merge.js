//Write a function that takes two sorted lists of numbers and merges them into a single sorted list.

let a = [ 1, 3, 7, 9, 10 ];
let b = [ 2, 4, 6, 8 ];

function mergeSorted(a, b) {
  let res = [];

  while( a.length && b.length ) {
    if ( a[0] < b[0] ) {
      res.push(a.shift());
    } else {
      res.push(b.shift());
    }
  }

  return res.concat(a, b);
}

console.log(mergeSorted(a,b));

// Proper linked lists
function LinkedList(node) {
  this.head = node || null;
  this.tail = node || null;
};

LinkedList.prototype.insert = function(n) {
  if (!this.head) {
    this.head = n;
    this.tail = this.head;
  } else {
    this.tail.next = n;
    this.tail = n;
  }
}

LinkedList.prototype.toString = function() {
  let p = this.head, out = '';
  while(p.next) {
    out += p.val + ' -> ';
    p = p.next;
  }
  out += p.val;
  return out;
};

function ListNode(val) {
  this.val = val;
  this.next = null;
}

function createList(a) {
  return a.reduce((list, cur) => {
    if (!list) return new LinkedList(new ListNode(cur));
    list.tail.next = new ListNode(cur);
    list.tail = list.tail.next;
    return list;
  }, null);
};

function mergeLinked(a, b) {
  let ret = new LinkedList();
  while(a.head && b.head) {
    if (a.head.val < b.head.val) {
      ret.insert(a.head);
      a.head = a.head.next;
    } else {
      ret.insert(b.head);
      b.head = b.head.next;
    }
  }

  if (a.head) {
    ret.tail.next = a.head;
    ret.tail = a.tail;
  }
  if (b.head) {
    ret.tail.next = b.head;
    ret.tail = b.tail;
  }

  return ret;
}

const aList = createList([ 1, 3, 7, 9, 10 ]);
const bList = createList([ 2, 4, 6, 8 ]);

console.log(mergeLinked(aList, bList).toString());
