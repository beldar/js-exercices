// Rearrange a string so that all same characters become d distance away
// Given a string and a positive integer d. Some characters may be repeated
//  in the given string. Rearrange characters of the given string such that
//  the same characters become d distance away from each other.
//  Note that there can be many possible rearrangements,
//  the output should be one of the possible rearrangements.
//  If no such arrangement is possible, that should also be reported.

function rearrange(s, d) {
  let ret = Array(s.length).fill(null),
      freq, done = {};

  freq = s.split('').reduce((prev, cur) => {
    if (!done[cur]) {
      let r = new RegExp(cur, 'gi'),
          f = s.match(r).length;

      prev[f] = prev[f] || [];
      prev[f].push(cur);
      done[cur] = true;
    }
    return prev;
  }, {});

  let freqs = Object.keys(freq).sort();

  while(freqs.length) {
    let nextFreq = freqs.pop();
    let nextFreqs = freq[nextFreq];
    for (let j = 0;j < nextFreqs.length;j++) {
      let char = nextFreqs[j];
      nextPos = ret.indexOf(null);
      for(let i = 0;i < nextFreq;i++) {
        if (nextPos === -1) return 'Arrangement not possible!';
        if (ret[nextPos] !== null) {
          while (ret[nextPos] !== null && nextPos < s.length) nextPos += d+1;
          if (ret[nextPos] !== null) return 'Arrangement not possible!';
        }
        ret[nextPos] = char;
        nextPos += d+1;
      }
    }
  }

  return ret.join('');
}

console.log(rearrange('aabc', 2));
console.log(rearrange('abbcdeff', 1));
console.log(rearrange('aaabbffcdeolp', 2));
