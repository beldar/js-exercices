// Route Between Nodes: Given a directed graph,
// design an algorithm to find out whether there is a route between two nodes.

const Graph = require('../data-structures/graph');

function routeExists(g, a, b) {
  //Implemented with BFS

  let visited = {}, q = [];

  q.push(a);

  while(q.length) {
    let cur = q.shift();
    let adj = g.nodes[cur];

    for(let i = 0;i < adj.length;i++) {
      let v = adj[i];
      if (v === b) return true;
      if (!visited[v]) {
        visited[v] = true;
        q.push(v);
      }
    }
  }

  return false;
};

const g = Graph();

g.addEdge(0)
.addEdge(1)
.addEdge(2)
.addEdge(3)
.addEdge(4)
.addEdge(5);

g.addVertex(0,1)
.addVertex(0,4)
.addVertex(0,5)
.addVertex(1,3)
.addVertex(1,4)
.addVertex(2,1)
.addVertex(3,2)
.addVertex(3,4);

console.log(routeExists(g, 0, 3));
