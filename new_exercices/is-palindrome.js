//Determine if a given string is a palindrome.

function isPalindrome(s) {
  let i = 0,
      j = s.length-1,
      isPal = true;

  while(i <= j) {
    if (s[i] !== s[j]) {
      isPal = false;
      break;
    }
    i++;
    j--;
  }

  return isPal;
}

console.log(isPalindrome('atacata'));
console.log(isPalindrome('aratara'));
console.log(isPalindrome('araara'));
console.log(isPalindrome('arrara'));
