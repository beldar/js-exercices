//Find a commonality
//-In an org, given two employees, find their common manager

const org = {
  key: 'CEO',
  children: [
    {
      key: 'CTO',
      children: [
        {
          key: 'Head of Tech1',
          children: [
            {
              key: 'Dev Manager',
              children: [
                { key: 'Dev1', children: null},
                { key: 'Dev2', children: null},
                { key: 'Dev3', children: null},
                { key: 'Dev4', children: null},
                { key: 'Dev5', children: null},
              ]
            }
          ]
        }
      ]
    },
    {
      key: 'CMO',
      children: [
        {
          key: 'Head of Marketing',
          children:[
            {
              key: 'Marketing Exec',
              children: null
            }
          ]
        }
      ]
    }
  ]
};

const Node = function(key, parent) {
  this.key = key || null;
  this.parent = parent || null;
  this.children = null;
};

const buildTree = function(org) {
  function build(nodeData, parent) {
    let node = new Node(nodeData.key, parent);
    if (nodeData.children) {
      node.children = nodeData.children.map(nd => build(nd, node));
    }
    return node;
  }

  return build(org, null);
};

const findElement = function(target, node) {
  if ( !node ) return null;
  if ( node.key === target ) {
    return node;
  } else if (node.children && node.children.length){
    for(let i = 0; i < node.children.length; i++) {
      let n = findElement(target, node.children[i]);
      if (n) return n;
    }
  }

  return null;
};

const findCommonManager = function(tree, a, b) {
  //Worst case O(d(a)+d(b)) where d is the depth of node
  const getPath = function(node) {
    let path = {};

    while(node!==null) {
      path[node.key] = true;
      node = node.parent;
    }

    return path;
  };

  const pathA = getPath(a);
  const pathB = getPath(b);

  let common = Object.keys(pathA).filter(v => pathB[v] && v!==a.key && v!==b.key);

  if ( common && common.length ) return common[0];
  return -1;
};

const findCommonManager1 = function(tree, a, b) {
  //Worst case O(d(a)+d(b)) where d is the depth of node

  const getPath = function(node, other) {
    let path = {};

    while(node!==null) {
      if (other
       && other[node.key]
       && node.key !== a.key
       && node.key !== b.key)
       return node.key;
      path[node.key] = true;
      node = node.parent;
    }

    return other ? null : path;
  };

  const pathA = getPath(a);
  const match = getPath(b, pathA);

  return match ? match : -1;
};

function findCommonManager2(tree, a, b){
  const depth = (n) => {
    let d = 0;
    while(n) {
      n = n.parent;
      d++;
    }
    return d;
  };

  const goUpBy = (n, d) => {
    while(d && n) {
      n = n.parent;
      d--;
    }
    return n;
  }

  let d = depth(a) - depth(b);
  let first = d > 0 ? b : a; //shallow
  let second = d > 0 ? a : b; //deep

  second = goUpBy(second, Math.abs(d));

  while (
       first
    && second
    && (( first === a || first === b ) || first !== second )
    && (( second === a || second === b ) || first !== second )
  ) {
    first = first.parent;
    second = second.parent;
  }

  return !first && !second ? null : first.key;
}

const assert = require('assert');

const tree = buildTree(org);

const dev2 = findElement('Dev2', tree);
const markExec = findElement('Marketing Exec', tree);
const dev1 = findElement('Dev1', tree);
const devManager = findElement('Dev Manager', tree);

console.log('v0', findCommonManager(tree, dev2, markExec));
console.log('v1', findCommonManager1(tree, dev2, markExec));
console.log('v2', findCommonManager2(tree, dev2, markExec));
console.log('v0', findCommonManager(tree, dev2, dev1));
console.log('v1', findCommonManager1(tree, dev2, dev1));
console.log('v2', findCommonManager2(tree, dev2, dev1));

// I this case its not clear what the behaviour should be
// When one of the nodes is already an ancestor of the other one
// My interpretation is that the result should never be one of the inputs
console.log('v0', findCommonManager(tree, dev2, devManager));
console.log('v1', findCommonManager1(tree, dev2, devManager));
console.log('v2', findCommonManager2(tree, dev2, devManager));

console.log('Run times');
console.time('v0');
findCommonManager(tree, dev2, markExec);
console.timeEnd('v0');

console.time('v1');
findCommonManager1(tree, dev2, markExec);
console.timeEnd('v1');

console.time('v2');
findCommonManager2(tree, dev2, markExec);
console.timeEnd('v2');

assert.equal(findCommonManager(tree, dev2, markExec), 'CEO', 'Common manager between Dev2 and Marketing Exec');
assert.equal(findCommonManager(tree, dev2, dev1), 'Dev Manager', 'Common manager between Dev2 and Dev1');
assert.equal(findCommonManager(tree, dev2, devManager), 'Head of Tech1', 'Common manager between Dev2 and Dev Manager');
