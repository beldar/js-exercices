// Find the maximum subarray, that is the longest continuous succession
// of numbers in an array which the sum of them is the maximum sum inside the array

// https://en.wikipedia.org/wiki/Maximum_subarray_problem
const a = [13,-3,-25,20,-3,-16,-23,18,20,-7,12,-5,-22,15,-4,7];

function maxSubarray(a) {
  let tmax = a[0], max = a[0], sub = [a[0]], submax = [a[0]];

  for(let i = 1;i < a.length; i++) {
    if ( tmax + a[i] > a[i]) {
      tmax = tmax + a[i];
      sub.push(a[i]);
    } else {
      tmax = a[i];
      sub = [a[i]];
    }

    if ( tmax > max ) {
      max = tmax;
      submax = sub.slice();
    }
  }

  return {max, submax};
}

console.log(maxSubarray(a));
