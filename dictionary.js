// You have a dictionary which is an array of words and array of strings.
//
// Write two functions
//
// 1. Prepare the array of strings to be searched in the dictionary
// 2. Check if the string contains all valid words or not.

function prepareDict(a) {
  return a.reduce((prev, cur) => {
    prev[cur] = true;
    return prev;
  }, {});
};

function checkStrings(a, dict) {
  function checkString(str) {
    let words = str.split(' ');
    for (let i = 0;i < words.length;i++) {
      if (!dict[words[i]]) {
        return false;
      }
    }

    return true;
  };

  for (let i = 0;i < a.length;i++) {
    if (!checkString(a[i])) {
      return false;
    }
  }

  return true;
};

const dict = ['hello', 'world'];
const strings = ['hello world', 'world hello', 'hello world lala'];

console.log(checkStrings(strings, prepareDict(dict)));
