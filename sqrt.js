// Implement a square root function.


//babylonian method

function sqrt(n, guess) {
  if (!guess) {
    guess = n/2.0;
  }

  let newGuess = (n/guess + guess)/2.0;

  if (guess == newGuess) return guess;

  return sqrt(n, newGuess);
}

console.log(sqrt(47));
