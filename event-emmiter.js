//Write an emitter class:
/* emitter = new Emitter();
// 1. Support subscribing to events.
sub = emitter.subscribe('event_name', callback);
sub2 = emitter.subscribe('event_name', callback2);

// 2. Support emitting events.
// This particular example should lead to the `callback` above
being invoked with `foo` and `bar` as parameters.
emitter.emit('event_name', foo, bar);

// 3. Support unsubscribing existing subscriptions by releasing them.
sub.release();

// `sub` is the reference returned by `subscribe` above */

const Emitter = function() {
  this.events = {}
};

Emitter.prototype.subscribe = function( eName, cb ) {
  this.events[eName] = this.events[eName] || [];
  this.events[eName].push(cb);

  return {
    release: () => {
      let i = this.events[eName].indexOf(cb);
      this.events[eName].splice(i, 1);
      if (!this.events[eName].length) delete this.events[eName];
    }
  }
};

Emitter.prototype.emit = function( eName, ...args ) {
  if ( !this.events[eName] ) throw new Error(`Event ${eName} doesn't exist`);
  this.events[eName].forEach(el => el.call(this, ...args));
};

var emitter = new Emitter();
var sub = emitter.subscribe('event_name', (a, b) => console.log('cb1 ', a, b));
var sub2 = emitter.subscribe('event_name', (a, b) => console.log('cb2 ', a, b));
emitter.emit('event_name', 'foo', 'bar');
sub.release();
sub2.release();
console.log(emitter);
