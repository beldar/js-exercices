// Can you write a function that deeply flattens an array?

const a = [[1, 2, 3], "", [], [{a:4}, [5, undefined, [6]], 7], 8];

function flattenArray( a ) {
  let result = [];

  a.forEach(el => flatten(el, result));
  return result;
}

function flatten(a, result) {
  if (!Array.isArray(a)) return result.push(a);
  a.forEach(el => flatten(el, result));
}

function flattenRecursive( a, ret ) {
  return a.reduce((ret, cur) => {
    if (Array.isArray(cur) && cur.length) {
      flattenRecursive(cur, ret);
    } else {
      ret.push(cur);
    }
    return ret;
  }, ret || [])
}

function flattenIterative( a ) {
  let ret = [];

  while( a.length ) {
    let cur = a.pop();
    if (Array.isArray(cur) && cur.length) {
      a = [...a, ...cur];
    } else {
      ret.push(cur)
    }
  }

  return ret.reverse();
}

console.time('flattenArray');
console.log(flattenArray(a));
console.timeEnd('flattenArray');

console.time('flattenRecursive');
console.log(flattenRecursive(a));
console.timeEnd('flattenRecursive');

console.time('flattenIterative');
console.log(flattenIterative(a));
console.timeEnd('flattenIterative');
