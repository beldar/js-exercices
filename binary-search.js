// Implement a Binary Search algorithm to find a value in an array
// https://en.wikipedia.org/wiki/Binary_search_algorithm

function binarySearch(a, target) {
  let min = 0;
  let max = a.length - 1;
  let mid;

  while (min <= r) {
    mid = Math.floor((min+max)/2);
    if (a[mid] < target) {
      min = mid+1;
    }
    else if (a[mid] > target ) {
      max = mid-1;
    }
    else return mid;
  }

  return -1;
}

const a = [0, 1, 2, 3, 4, 5];
const target = 0;

console.log(binarySearch(a, target));
