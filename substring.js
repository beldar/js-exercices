// Given a string String="ABCSC" Check whether it contains a Substring="ABC"?
//
// 1)If no , return "-1".
// 2)If yes , remove the substring from string and return "SC".
// use very simple code and concept(ALGORITHM)

function findSubstr(string, substring) {
  let index = string.indexOf(substring);

  if (index === -1) return index;

  if (index === 0) {
    return string.substr(index+substring.length);
  }

  return string.substr(0, index) + string.substr(index+substring.length);
}

console.log(findSubstr('ABCSC', 'ABC'));
console.log(findSubstr('AAAAAABCSSSSC', 'ABC'));
console.log(findSubstr('AAAAAABC', 'ABC'));

// function findSubstrIndex(string, substr) {
//   let index = -1, acc = [], sublen = substr.length, tmpI = 0;
//
//   for (let i = 0;i < string.length;i++) {
//     let char = string[i];
//     console.log(char, substr[tmpI],tmpI, acc);
//     if (substr[tmpI] === char) {
//       if (index === -1) index = i;
//       acc.push(char);
//       tmpI++;
//       if (acc.length === sublen) return index;
//     } else {
//       index = -1;
//       tmpI = 0;
//       acc = [];
//     }
//   }
//
//   return -1;
// }
//
// console.log(findSubstrIndex('ABCSC', 'ABC'), 'ABCSC'.indexOf('ABC'));
// console.log(findSubstrIndex('AAAAABCSSSSC', 'ABC'), 'AAAAABCSSSSC'.indexOf('ABC'));
// console.log(findSubstrIndex('AAAAAABC', 'ABC'), 'AAAAAABC'.indexOf('ABC'));
