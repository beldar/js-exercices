// If you were building a search tool and wanted search results
// to pop up as you typed but the server call was taxing,
// write a function that gets called on every key down but
// calls the server when the user stops typing for 400ms.

function debounce(fn, wait) {
  let timeout = null;

  return function() {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      fn.apply(this, arguments)
    }, wait);
  };
};

var i = 0;
var dbounced = debounce(function(t){ console.log(t, ++i)}, 200);

var interval = setInterval(() => {
  i++;
  dbounced('lalal')
  if ( i === 100) {
    clearInterval(interval);
    setTimeout(() => dbounced('lololo'), 400);
  }
}, 100);
