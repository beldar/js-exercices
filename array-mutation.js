// Given an input array and another array that describes a new
// index for each element, mutate the input array so that each element ends up in their new index.
// Discuss the runtime of the algorithm and how you can be sure there won't be any infinite loops.

const input = [1, 2, 3, 4];
const indices = [1, 3, 0, 2]; //[3,1,4,2]

function mutate(input, indices) {
  const visited = Array(indices.length).fill(false);

  const swap = (i, j) => {
    let tmp = input[i];
    input[i] = input[j];
    input[j] = tmp;
  };

  indices.forEach((newIndex, i) => {
    visited[newIndex] = true;

    if (!visited[i]) {
      swap(newIndex, i);
    }
  });

  return input;
}


console.time('mutate');
console.log(mutate(input, indices));
console.timeEnd('mutate');
