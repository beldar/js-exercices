//Exercise 2

Array(5).fill('x').join();

repeat('x', 4); // 'xxxxx'

function repeat(char, times) {
  if (typeof char !== 'string') throw new Error('Not a string!');
  if (isNaN(times) || times < 1) throw new Error('Not a positive number!');

  let str = char;

  while(str.length < times) {
    str += str;
  }

  return str.length === times ? str : str.substr(0, times);
}

//http://stackoverflow.com/a/5450113/1089391
//http://stackoverflow.com/a/17800645/1089391
