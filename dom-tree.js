//Given a node from a DOM tree find the node in the same position from an identical DOM tree.

//JS
let nodeA = document.querySelectorAll('#a ul li:last-child')[0];
let rootA = document.getElementById('a');
let rootB = document.getElementById('b');

const getPath = function(node, root) {
	const path = [];
  let parentNode;
	while(node !== root) {
  	parent = node.parentNode;
    path.push(Array.from(parent.childNodes).indexOf(node));
    node = parent;
  }

	return path;
};

const followPath = function(node, path) {
  while(path.length) {
  	node = node.childNodes[path.pop()];
  }

  return node;
};

let nodeB = followPath(rootB, getPath(nodeA, rootA));
console.log(nodeB)



//HTML
/*

<div id="a">
  <ul>
  <li>one</li>
  <li>two</li>
  <li>three</li>
  </ul>
</div>
<div id="b">
  <ul>
  <li>one</li>
  <li>two</li>
  <li>three</li>
  </ul>
</div>

 */
